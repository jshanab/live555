This is a side-by-side JFS Technologies CMake meta-project.

CMake always builds "out of source", i.e. you make a directory, change into it then call cmake on the project you want built.
 - This keeps the makefiles, solutions and all the build crap out of your source directories.
 - I go one step further and create a directory of cmake mimicking the src in a peer directory.
   - This keeps the CMake out of the source, you are safe to delete and untar fresh source when updating!

The prep.py script is just for convenience. 

Basically you will
  Set the JFS_BUILD_ROOT env var to where you want to build (or set -p arg in prep.py)
  Download the tar into src.
  (fix permissions if osx)
  untar in src to get src/live
  cd to the liblive555 directory 
  type prep -h 
    then type prep -x64 etc following help from prep -h
    I type "python prep.py -x64" on osx.

OSX 
  - What started out as the best of both worlds, has taken a strange turn, Being a developer can be quite painful on OSX.
    - Download the tar from live networks and apple tags it as downloaded from the internet and quarantines.
    - Unarchive it and it does not ask it just tags ALL files as quarantined and from the internet 
      preventing editing and warning you every time you read it.
      - xattr -l live.2016.02.22.tar.gz to see this
      - xattr -c live.2016.02.22.tar.gz to clear this crap BEFORE you untar it!

Windows
  - Linux by default exports all symbols in a lib, windows exports none.
  - Ross does not add the typical dllexport just for windows so we need a def file.
    - You must have one for 64bit and one for 32 bit.
    - The one here has the minimal needed for the test applications in the project because they are a pain to write.
    - Visual Studio will give you an error with enough info to add new ones as you find you need them.

The tarfile creates the live directory and everything below it in the src directory. This meta project assumes you have done this.      
