import sys, os
from sys import platform as _platform
import getopt
import platform

# usage: prep -h,--help  get help
# usage: prep just create 32 bit project
# usage: prep -x64 create 64 bit project
def main(argv):
    pretend = False
    bitness = 32
    gKey = 'vs10_x64'
    branch = "none"
    # 0 means n/a, bits can be selected at build time.
    WindowsGenerators = {   "vs8":('"Visual Studio 8 2005"',32),
                            "vs8_x64":('"Visual Studio 8 2005 Win64"',64),
                            "vs9":('"Visual Studio 9 2008"',32),
                            "vs9_x64":('"Visual Studio 9 2008 Win64"',64),
                            "vs9_ia64":('"Visual Studio 9 2008 IA64"',64),
                            "vs10":('"Visual Studio 10 2010"',32),
                            "vs10_x64":('"Visual Studio 10 2010 Win64"',64),
                            "vs10_ia64":('"Visual Studio 10 2010 IA64"',64),
                            "vs11":('"Visual Studio 11 2012"',32),
                            "vs11_x64":('"Visual Studio 11 2012 Win64"',64),
                            "vs11_arm":('"Visual Studio 11 2012 ARM"',0),
                            "vs12":('"Visual Studio 12 2013"',32),
                            "vs12_x64":('"Visual Studio 12 2013 Win64"',64),
                            "vs12_arm":('"Visual Studio 12 2013 ARM"',0),
                            "vs14":('"Visual Studio 14 2015"',32),
                            "vs14_x64":('"Visual Studio 14 2015 Win64"',64),
                            "vs14_arm":('"Visual Studio 14 2015 ARM"',0),
                            "vs15":('"Visual Studio 15 2017"',32),
                            "vs15_x64":('"Visual Studio 15 2017 Win64"',64),
                            "vs15_arm":('"Visual Studio 15 2017 ARM"',0),
                            "ninja":('"Ninja"',0),
                            "borland":('"Borland Makefiles"',0),
                            "msys":('"MSYS Makefiles"',0),
                            "ming":('"MinGW Makefiles"',0),
                            "nmake":('"NMake Makefiles"',0),
                            "watcom":('"Watcom WMake"',0)}

    LinuxGenerators = { "unix":('"UNIX Makefiles"',0),
                        "ninja":('"Ninja"',0),
                        "watcom":('"Watcom WMake"',0)}

    osxGenerators = {"xcode":('"Xcode"',0)}
                            

    try:
        opts, args = getopt.getopt(argv,"phg:b:")
    except getopt.GetoptError:
        print ('prep.py -h')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print ('================== G e n e r a t o r s ==============================================')
            if platform.system() == 'Windows':
                for nick,generator in sorted(WindowsGenerators.items()):
                    print ( "   %-20s      : %s" % (nick,generator[0]))
            if platform.system() == 'Linux':
                for nick,generator in sorted(LinuxGenerators.items()):
                    print ( "   %-20s      : %s" % (nick,generator[0]))
            if platform.system() == 'Darwin':
                for nick,generator in sorted(osxGenerators.items()):
                    print ( "   %-20s      : %s" % (nick,generator[0]))
            print ('  ')
            print (' note: windowsCE generators not supported in this script.')
            print ('prep.py | prep.py -g nick')
            sys.exit()
        elif opt in ("-g"):
            gKey = arg
        elif opt in ("-p"):
            pretend = True
        elif opt in ("-b"):
            branch = arg

    BuildRoot = os.environ.get('EXACQ_BUILD_ROOT')
    if _platform == "linux" or _platform == "linux2":
        GENERATOR="UNIX Makefiles"
        if gKey:
            GENERATOR,bitness = LinuxGenerators[gKey]
    elif _platform == "darwin":
        GENERATOR,bitness = osxGenerators[gKey]
    elif _platform == "win32":
        if gKey:
            GENERATOR,bitness = WindowsGenerators[gKey]
    
    if pretend == True:
        print ('___________________________________________')
        print ('          P r e t e n d i n g ')
        print ('^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^')
    print('')
    print ('----------------------------------------------------------vvvv Prep.py')
    print (' Generator set to %s' % GENERATOR )
    print (' Bittness = %d ' % bitness )
    
      

    if BuildRoot is None:
        print ('============================================================================')
        print (' ERROR: EXACQ_BUILD_ROOT not found')
        print ('')
        print ('    CMake builds generate the project into a local build directory')
        print (' which is kept out of the source tree.')
        print ('')
        print (' This requires the environment variable EXACQ_BUILD_ROOT to be set ')
        print ('')  
        if _platform == "linux" or _platform == "linux2":
            print ('Please create a build directory and ') 
            print ('   export EXACQ_BUILD_ROOT=/myLocalBuild  before calling this script')
            print ('-or-')
            print (' put the export statment in your .bashrc and reopen/reload your terminal')
        elif _platform == "darwin":
            print (' do whatever you mac guys do to set an environment variable')
        elif _platform == "win32":
            print ('    Please set the environment variable EXACQ_BUILD_ROOT in ')
            print (' computer->properties->advanced->System Environment variables ')
        print ('============================================================================')
        
        sys.exit(1)

    pathparts = os.path.split(os.path.abspath(sys.path[0]))
    if bitness == 64:
        pathsuffix = pathparts[1] + "_x64"
    else:
        pathsuffix = pathparts[1]

    BuildPath = os.path.join(BuildRoot, pathsuffix)

    if not os.path.isdir(BuildPath):
        print ('Creating build directory ' + BuildPath)
        os.makedirs(BuildPath)

    print (' Prepparing project into ' + BuildPath)
    print ('----------------------------------------------------------^^^^ Prep.py')
    print ('')
    sourceDir = os.getcwd()
    try:
        os.chdir(BuildPath)
    except:
        print ('Could not change to build directory: aborting')
        os.chdir(sourceDir)
        sys.exit(2)

    #@TODO create a python module for this.
    ########## Environment ###########
    if _platform == "linux" or _platform == "linux2":
        nop = 1
        # do linux eqivilent to SET,SETX
    elif _platform == "darwin":
        nop = 2
        # do apple eqivilent to SET,SETX
    elif _platform == "win32":
        os.environ['BOOSTROOT_x86'] = 'C:/opt (x86)/boost/1.54.0'
        os.environ['BOOSTROOT_x64'] = 'C:/opt/boost/1.54.0'
    ########## Environment ###########

    os.environ['BUILD_ARCH'] = 'x32' 

    
    #cmakeCommand = 'cmake -DBoost_DEBUG=ON ' + '-G' + GENERATOR + ' ' + sourceDir
    if  branch != "none":
        cmakeCommand = 'cmake ' + "-DREPO_NAME:STRING=" + branch + ' -G' + GENERATOR + ' ' + sourceDir
    else:
        cmakeCommand = 'cmake ' + '-G' + GENERATOR + ' ' + sourceDir

    #cmakeCommand = 'cmake ' + '--trace --debug-output' + '-G' + GENERATOR + ' ' + sourceDir

    if pretend == True:
        print (cmakeCommand)
    else:
        os.system(cmakeCommand)

    #os.chdir(sourceDir)



if __name__ == "__main__":
    main(sys.argv[1:])

