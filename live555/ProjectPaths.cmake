#/**********************************************************************\ 
#
# liblive555 Paths and set vars that must come after a project is defined.
# 
#\**********************************************************************/

# if this is a project in a branch, extract the branch for access to peer directories
MESSAGE( "Setting ${PROJECT_NAME} project options from ProjectConfig.cmake")
get_filename_component(BRANCH_DIR ${CMAKE_CURRENT_SOURCE_DIR} DIRECTORY)

SET(PRODUCT_DIR ${BRANCH_DIR}/${PROJECT_NAME} )
SET(PRODUCT_BIN_DIR "${CMAKE_BINARY_DIR}" )

SET(BITNESS 32 CACHE STRING "32 vs 64 bit build")

#install prefix
if( ${CMAKE_GENERATOR} MATCHES "^Visual Studio 10 2010$")
    SET( BITNESS 32)
    SET( ARCH "x86")
    SET( OPT_SUFFIX "opt (x86)")
    SET( OS_TARGET "Windows")
    SET( TOOLSET   "vc10")
ENDIF()

if( ${CMAKE_GENERATOR} MATCHES "^Visual Studio 10 2010 Win64$")
    SET( BITNESS 64)
    SET( ARCH "x64")
    SET( OPT_SUFFIX "opt")
    SET( OS_TARGET "Windows")
    SET( TOOLSET   "vc10")
ENDIF()

if( ${CMAKE_GENERATOR} MATCHES "^Unix Makefiles$")
    if( ${BITNESS} MATCHES "32")
        SET( ARCH "x86")
        SET( OPT_SUFFIX "opt")
        SET( OS_TARGET "linux")
        SET( TOOLSET   "gcc")
    ELSE()
        SET( ARCH "x64")
        SET( OPT_SUFFIX "opt")
        SET( OS_TARGET "linux")
        SET( TOOLSET   "gcc")
        add_definitions( -DBSD=1 -m64  -fPIC -I. -O2 -DSOCKLEN_T=socklen_t -D_LARGEFILE_SOURCE=1 -D_FILE_OFFSET_BITS=64)
    ENDIF()
ENDIF()

if( ${CMAKE_GENERATOR} MATCHES "^Xcode$")
    SET( BITNESS 64)
    SET( ARCH "x64")
    SET( OPT_SUFFIX "opt")
    SET( OS_TARGET "OSX")
    SET( TOOLSET   "clang")
    set(CMAKE_CXX_FLAGS "-std=c++11 -stdlib=libc++")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -stdlib=libc++")
    add_definitions(-DBSD=1 -O -DSOCKLEN_T=socklen_t -DHAVE_SOCKADDR_LEN=1 -DTIME_BASE=int)

ENDIF()

SET( PROJECT_INSTALL_PREFIX "${PRODUCT_BIN_DIR}/${OPT_SUFFIX}/${PROJECT_NAME}/${VERSION_STRING}")
MESSAGE("  ${BITNESS}bit install prefix set for ${OS_TARGET} : ${PROJECT_INSTALL_PREFIX}")

SET(CMAKE_INSTALL_PREFIX ${PROJECT_INSTALL_PREFIX})

SET (LIVE555_ROOT ${BRANCH_DIR}/src/live)

set( ICONS_DIR "${CMAKE_SOURCE_DIR}/images")
SET( LICENSE_FILE "License.txt" )
SET( README_FILE "ReadMe.txt" )


