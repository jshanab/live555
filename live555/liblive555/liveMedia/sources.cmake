
SET ( LIVEMEDIA_SOURCE_DIR ${LIVE555_ROOT}/liveMedia)

FILE(GLOB LIVEMEDIA_SOURCES 
     "${LIVEMEDIA_SOURCE_DIR}/*.cpp"
     "${LIVEMEDIA_SOURCE_DIR}/*.c"
     )

# for reference, here is how to exclude a few
#LIST(REMOVE_ITEM Common_CPP ${Mac_CPP} ${Win32_CPP} ${Linux_CPP})


