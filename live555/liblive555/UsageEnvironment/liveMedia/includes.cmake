

SET ( LIVEMEDIA_INCLUDE_DIR ${LIVE555_ROOT}/liveMedia/include CACHE INTERNAL DocString)

include_directories(${LIVEMEDIA_INCLUDE_DIR})

#why not file glob.
# while usually frowned on becasue it breaks source code version tracking, ie what you added and what you removed, 
# it may be a good idea on a library we just take from upstream
# on the other hand, being excplicit allows us to create a custom lib, ie client only if we want.

SET ( LIVEMEDIA_HEADERS
      "${LIVE555_ROOT}/liveMedia/rtcp_from_spec.h"
      "${LIVE555_ROOT}/liveMedia/EBMLNumber.hh"
      "${LIVE555_ROOT}/liveMedia/H263plusVideoStreamParser.hh"
      "${LIVE555_ROOT}/liveMedia/MP3ADUdescriptor.hh"
      "${LIVE555_ROOT}/liveMedia/MP3AudioMatroskaFileServerMediaSubsession.hh"
      "${LIVE555_ROOT}/liveMedia/MP3Internals.hh"
      "${LIVE555_ROOT}/liveMedia/MP3InternalsHuffman.hh"
      "${LIVE555_ROOT}/liveMedia/MP3StreamState.hh"
      "${LIVE555_ROOT}/liveMedia/MPEGVideoStreamParser.hh"
      "${LIVE555_ROOT}/liveMedia/MatroskaDemuxedTrack.hh"
      "${LIVE555_ROOT}/liveMedia/MatroskaFileParser.hh"
      "${LIVE555_ROOT}/liveMedia/MatroskaFileServerMediaSubsession.hh"
      "${LIVE555_ROOT}/liveMedia/OggDemuxedTrack.hh"
      "${LIVE555_ROOT}/liveMedia/OggFileParser.hh"
      "${LIVE555_ROOT}/liveMedia/OggFileServerMediaSubsession.hh"
      "${LIVE555_ROOT}/liveMedia/StreamParser.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/AC3AudioFileServerMediaSubsession.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/AC3AudioRTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/AC3AudioRTPSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/AC3AudioStreamFramer.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/ADTSAudioFileServerMediaSubsession.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/ADTSAudioFileSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/AMRAudioFileServerMediaSubsession.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/AMRAudioFileSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/AMRAudioFileSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/AMRAudioRTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/AMRAudioRTPSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/AMRAudioSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/AVIFileSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/AudioInputDevice.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/AudioRTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/Base64.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/BasicUDPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/BasicUDPSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/BitVector.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/ByteStreamFileSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/ByteStreamMemoryBufferSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/ByteStreamMultiFileSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/DVVideoFileServerMediaSubsession.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/DVVideoRTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/DVVideoRTPSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/DVVideoStreamFramer.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/DeviceSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/DigestAuthentication.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/FileServerMediaSubsession.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/FileSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/FramedFileSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/FramedFilter.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/FramedSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/GSMAudioRTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/GenericMediaServer.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/H261VideoRTPSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/H263plusVideoFileServerMediaSubsession.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/H263plusVideoRTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/H263plusVideoRTPSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/H263plusVideoStreamFramer.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/H264VideoFileServerMediaSubsession.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/H264VideoFileSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/H264VideoRTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/H264VideoRTPSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/H264VideoStreamDiscreteFramer.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/H264VideoStreamFramer.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/H264or5VideoFileSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/H264or5VideoRTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/H264or5VideoStreamDiscreteFramer.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/H264or5VideoStreamFramer.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/H265VideoFileServerMediaSubsession.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/H265VideoFileSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/H265VideoRTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/H265VideoRTPSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/H265VideoStreamDiscreteFramer.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/H265VideoStreamFramer.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/InputFile.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/JPEGVideoRTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/JPEGVideoRTPSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/JPEGVideoSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/Locale.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MP3ADU.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MP3ADURTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MP3ADURTPSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MP3ADUTranscoder.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MP3ADUinterleaving.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MP3AudioFileServerMediaSubsession.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MP3FileSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MP3Transcoder.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG1or2AudioRTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG1or2AudioRTPSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG1or2AudioStreamFramer.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG1or2Demux.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG1or2DemuxedElementaryStream.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG1or2DemuxedServerMediaSubsession.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG1or2FileServerDemux.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG1or2VideoFileServerMediaSubsession.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG1or2VideoRTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG1or2VideoRTPSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG1or2VideoStreamDiscreteFramer.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG1or2VideoStreamFramer.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG2IndexFromTransportStream.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG2TransportFileServerMediaSubsession.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG2TransportStreamFramer.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG2TransportStreamFromESSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG2TransportStreamFromPESSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG2TransportStreamIndexFile.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG2TransportStreamMultiplexor.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG2TransportStreamTrickModeFilter.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG2TransportUDPServerMediaSubsession.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG4ESVideoRTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG4ESVideoRTPSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG4GenericRTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG4GenericRTPSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG4LATMAudioRTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG4LATMAudioRTPSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG4VideoFileServerMediaSubsession.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG4VideoStreamDiscreteFramer.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEG4VideoStreamFramer.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MPEGVideoStreamFramer.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MatroskaFile.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MatroskaFileServerDemux.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/Media.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MediaSession.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MediaSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MediaSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MediaTranscodingTable.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MultiFramedRTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/MultiFramedRTPSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/OggFile.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/OggFileServerDemux.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/OggFileSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/OnDemandServerMediaSubsession.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/OutputFile.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/PassiveServerMediaSubsession.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/ProxyServerMediaSession.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/QCELPAudioRTPSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/QuickTimeFileSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/QuickTimeGenericRTPSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/RTCP.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/RTPInterface.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/RTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/RTPSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/RTSPClient.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/RTSPCommon.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/RTSPRegisterSender.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/RTSPServer.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/RTSPServerSupportingHTTPStreaming.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/SIPClient.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/ServerMediaSession.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/SimpleRTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/SimpleRTPSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/StreamReplicator.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/T140TextRTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/TCPStreamSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/TextRTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/TheoraVideoRTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/TheoraVideoRTPSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/VP8VideoRTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/VP8VideoRTPSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/VP9VideoRTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/VP9VideoRTPSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/VideoRTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/VorbisAudioRTPSink.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/VorbisAudioRTPSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/WAVAudioFileServerMediaSubsession.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/WAVAudioFileSource.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/liveMedia.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/liveMedia_version.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/ourMD5.hh"
      "${LIVEMEDIA_INCLUDE_DIR}/uLawAudioFilter.hh" 

    )

SET ( PUBLIC_LIVE555_INCLUDE_DIRS 
      ${PUBLIC_LIVE555_INCLUDE_DIRS}
      ${LIVEMEDIA_INCLUDE_DIR}
    )

