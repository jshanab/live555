
SET ( GROUPSOCK_SOURCE_DIR ${LIVE555_ROOT}/groupsock)

SET ( GROUPSOCK_SOURCES 
      "${GROUPSOCK_SOURCE_DIR}/GroupEId.cpp"
      "${GROUPSOCK_SOURCE_DIR}/Groupsock.cpp"
      "${GROUPSOCK_SOURCE_DIR}/inet.c"
      "${GROUPSOCK_SOURCE_DIR}/GroupsockHelper.cpp"
      "${GROUPSOCK_SOURCE_DIR}/IOHandlers.cpp"
      "${GROUPSOCK_SOURCE_DIR}/NetAddress.cpp"
      "${GROUPSOCK_SOURCE_DIR}/NetInterface.cpp"
    )
