
SET ( GROUPSOCK_INCLUDE_DIR ${LIVE555_ROOT}/groupsock/include CACHE INTERNAL DocString)

include_directories(${GROUPSOCK_INCLUDE_DIR})

SET ( GROUPSOCK_HEADERS 
      "${GROUPSOCK_INCLUDE_DIR}/GroupEId.hh"
      "${GROUPSOCK_INCLUDE_DIR}/Groupsock.hh"
      "${GROUPSOCK_INCLUDE_DIR}/groupsock_version.hh"
      "${GROUPSOCK_INCLUDE_DIR}/GroupsockHelper.hh"
      "${GROUPSOCK_INCLUDE_DIR}/IOHandlers.hh"
      "${GROUPSOCK_INCLUDE_DIR}/NetAddress.hh"
      "${GROUPSOCK_INCLUDE_DIR}/NetCommon.h"
      "${GROUPSOCK_INCLUDE_DIR}/NetInterface.hh"
      "${GROUPSOCK_INCLUDE_DIR}/TunnelEncaps.hh"
    )

SET ( PUBLIC_LIVE555_INCLUDE_DIRS 
      ${PUBLIC_LIVE555_INCLUDE_DIRS}
      ${GROUPSOCK_INCLUDE_DIR}
    )





