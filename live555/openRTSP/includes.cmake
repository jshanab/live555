
SET ( TESTPROGS_INCLUDE_DIR ${LIVE555_ROOT}/testProgs CACHE INTERNAL DocString)

include_directories(${TESTPROGS_INCLUDE_DIR})

SET ( OPENRTSP_HEADERS 
    "${TESTPROGS_INCLUDE_DIR}/playCommon.hh"
    "${BASICUSAGEENVIRONMENT_INCLUDE_DIR}/BasicUsageEnvironment.hh"
    "${GROUPSOCK_INCLUDE_DIR}/GroupsockHelper.hh"
    )


