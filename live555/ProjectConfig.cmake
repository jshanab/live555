#/******************************************************************\ 
#
# liblive555, Project variables for build
# 
#\******************************************************************/

set(PROJECT_NAME "live555")
set(COMPANY_NAME "Live Networks") 

SET(PRODUCT_DIR ${BRANCH_DIR}/${PROJECT_NAME} )
SET(PRODUCT_BIN_DIR "$ENV{JFS_BUILD_ROOT}/${PROJECT_NAME}" )

#@todo extract this from live555 
#
#  I decide to version the library using the system in live/config.linux-with-shared-libraries
#
#  # 'CURRENT':'REVISION':'AGE' are updated - whenever a library changes - as follows:
#  # The library code changes, but without any changes to the API (i.e., interfaces) => increment REVISION
#  # At least one interface changes, or is removed => CURRENT += 1; REVISION = 0; AGE = 0
#  # One or more interfaces were added, but no existing interfaces were changed or removed => CURRENT += 1; REVISION = 0; AGE += 1
# this instance
#  #libliveMedia_VERSION_CURRENT=57
#  #libliveMedia_VERSION_REVISION=3
#  #libliveMedia_VERSION_AGE=0

# Version System
set(MAJOR_VERSION_NUMBER "57")
set(MINOR_VERSION_NUMBER "3")
set(REVISION_NUMBER "0")
set(BUILD_NUMBER "1")
set(VERSION_STRING "${MAJOR_VERSION_NUMBER}.${MINOR_VERSION_NUMBER}.${REVISION_NUMBER}-${BUILD_NUMBER}")
set(PROJECT_NAMESPACE "${PROJECT_NAME}")

set( ICONS_DIR "${CMAKE_SOURCE_DIR}/Icons")
SET( LICENSE_FILE "License.txt" )
SET( README_FILE "ReadMe.txt" )

MACRO (TODAY RESULT)
    IF (WIN32)
        EXECUTE_PROCESS(COMMAND "cmd" " /C date /T" OUTPUT_VARIABLE ${RESULT})
        string(REGEX REPLACE ".* (..)/(..)/(....).*" "\\3\\1\\2" ${RESULT} ${${RESULT}})
    ELSEIF(UNIX)
        EXECUTE_PROCESS(COMMAND "date" "+%d/%m/%Y" OUTPUT_VARIABLE ${RESULT})
        string(REGEX REPLACE ".* (..)/(..)/(....).*" "\\3\\1\\2" ${RESULT} ${${RESULT}})
    ELSE (WIN32)
        MESSAGE(SEND_ERROR "date not implemented")
        SET(${RESULT} 000000)
    ENDIF (WIN32)
ENDMACRO (TODAY)

MACRO (ASSERT VARIABLE)
   if (NOT ${VARIABLE})
       message( "========================================" )
       message( FATAL_ERROR "||        Variable ${VARIABLE} is not set      ||")
       message( "========================================" )
   endif()
ENDMACRO(ASSERT)



